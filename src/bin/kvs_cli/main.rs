use clap::{self, load_yaml, App};
use dotenv_codegen::dotenv;
use std::env;

use kvs::{Error, KvStore, Result};

fn handle_error<T>(err: Error) -> T {
    println!("{}", err);

    std::process::exit(-1);
}

fn handle_get(kv_store: &KvStore, stub: &clap::ArgMatches) -> Result<()> {
    let key: String = stub.value_of("key").unwrap().to_string();

    let value = kv_store.get(&key)?;

    match value {
        Some(value) => println!("{}", value),
        None => println!("\"{}\" not found", key),
    };

    std::process::exit(0);
}

fn handle_set(kv_store: &KvStore, stub: &clap::ArgMatches) -> Result<()> {
    let key: String = stub.value_of("key").unwrap().to_string();
    let value: String = stub.value_of("value").unwrap().to_string();

    kv_store.set(key.clone(), value.clone())?;

    println!("Key \"{}\" set with value \"{}\"", key, value);

    std::process::exit(0);
}

fn handle_rm(kv_store: &KvStore, stub: &clap::ArgMatches) -> Result<()> {
    let key: String = stub.value_of("key").unwrap().to_string();

    kv_store.remove(key.clone())?;

    println!("Key \"{}\" removed", key);

    std::process::exit(0);
}

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml)
        .name("kvs - client")
        .version(dotenv!("CARGO_PKG_VERSION"))
        .author(dotenv!("AUTHOR"))
        .about(dotenv!("DESCRIPTION"))
        .get_matches();

    let current_dir = env::current_dir().unwrap();
    let kv_store = KvStore::open(&current_dir).unwrap();

    if matches.is_present("version") {
        println!("{}", dotenv!("CARGO_PKG_VERSION"));
        std::process::exit(0);
    }

    match matches.subcommand() {
        ("get", Some(stub)) => handle_get(&kv_store, &stub).unwrap_or_else(handle_error),
        ("set", Some(stub)) => handle_set(&kv_store, &stub).unwrap_or_else(handle_error),
        ("rm", Some(stub)) => handle_rm(&kv_store, &stub).unwrap_or_else(handle_error),
        _ => std::process::exit(1),
    };
}
