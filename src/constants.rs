pub mod file_name {
    pub const STATE: &str = ".state.kvs";
    pub const INDEX: &str = ".index.kvs";
    pub const PAGE_CAPACITY: &str = ".page_capacity.kvs";
}

pub const LOG_EXTENSION: &str = ".kvs.log";
