use crate::types;
use snafu::ResultExt;
use std::io::{self, prelude::*};

fn pad_buffer_to_len(buf: &mut Vec<u8>, len: usize) {
    if len > buf.len() {
        buf.resize(len, 0u8);
    }
}

pub fn attempt_write(writer: &mut dyn io::Write, bytes: &[u8]) -> types::Result<()> {
    let mut bytes_written = 0usize;
    let mut failed_write_attempts = 0u8;
    while bytes_written < bytes.len() {
        bytes_written += writer.write(bytes).context(types::Writing)?;
        failed_write_attempts += 1;

        if failed_write_attempts >= 3 {
            return types::WriteAllBytes {}.fail();
        }
    }

    Ok(())
}

pub fn attempt_read(
    reader: &mut io::Take<&mut dyn io::BufRead>,
    buffer: &mut Vec<u8>,
    target_len: usize,
) -> types::Result<()> {
    pad_buffer_to_len(buffer, target_len);
    reader.set_limit(target_len as u64);
    let mut bytes_read = 0usize;
    let mut failed_read_attempts = 0u8;
    while bytes_read < target_len as usize {
        bytes_read += reader
            .read(&mut buffer[bytes_read..])
            .context(types::Writing)?;
        failed_read_attempts += 1;

        if failed_read_attempts >= 3 {
            return types::ReadAllBytes {}.fail();
        }
    }

    Ok(())
}
