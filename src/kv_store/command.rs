#![allow(dead_code, unused_variables)]
use crate::{
    io_utils::{attempt_read, attempt_write},
    types,
};
use snafu::ResultExt;
use std::{convert::TryInto, io, str};

const TYPE_MASK: u32 = 0x0080_0000;
const KEY_LEN_MASK: u32 = 0x007F_FFFF;

/**
 * Serialization format table:
 *
 * BITS                     | PURPOSE
 * 0                        | Command Type (Set = 0, Remove = 1)
 * 1:23                     | Length of key in bytes (KL)
 * 24:((KL*8)-1)            | Key bytes
 * ------ BITS BELOW ARE ONLY IF VARIANT == SET ------
 * KL*8:(KL*8)+31           | Length of value in bytes (VL)
 * (KL*8)+32:((KL+VL)*8)+31 | Value bytes
 *
 * All values are big-endian.
 */
#[derive(Debug, Clone)]
pub enum Command {
    Set { key: String, value: String },
    Remove { key: String },
}
impl Command {
    pub fn serialize(&self, writer: &mut dyn io::Write) -> types::Result<usize> {
        let mut len_bytes = 3;

        let (key, value) = match self {
            Self::Set { key, value } => (key, Some(value)),
            Self::Remove { key } => (key, None),
        };

        let key_bytes: Vec<u8> = key.bytes().collect();
        let mut key_len = key_bytes.len() as u32;
        // TODO: Validate key length in bounds.
        key_len &= KEY_LEN_MASK;

        match self {
            Self::Set { key: _, value: _ } => {
                key_len |= TYPE_MASK;
            }
            Self::Remove { key: _ } => {
                key_len &= !TYPE_MASK;
            }
        };

        let key_len_bytes = key_len.to_be_bytes();
        attempt_write(writer, &key_len_bytes[1..])?;

        len_bytes += key_bytes.len();
        attempt_write(writer, &key_bytes)?;

        if let Some(value) = value {
            let value_bytes: Vec<u8> = value.bytes().collect();
            let value_len = value_bytes.len() as u32;

            len_bytes += 4;
            attempt_write(writer, &value_len.to_be_bytes())?;

            len_bytes += value_bytes.len();
            attempt_write(writer, &value_bytes)?;
        }

        Ok(len_bytes)
    }

    pub fn deserialize(
        handle: &mut io::Take<&mut dyn io::BufRead>,
        buf: &mut Vec<u8>,
    ) -> types::Result<Self> {
        attempt_read(handle, buf, 3)?;
        let key_len = u32::from_be_bytes([0, buf[0], buf[1], buf[2]]);
        let is_remove = key_len & TYPE_MASK == 0;
        let key_len: u32 = key_len & !TYPE_MASK;
        attempt_read(handle, buf, key_len as usize)?;

        let key = str::from_utf8(&buf[0..key_len as usize])
            .context(types::KeyValueInvalidUtf8)?
            .to_string();

        if is_remove {
            return Ok(Self::Remove { key });
        }

        attempt_read(handle, buf, 4)?;
        // If buffer length < 4, attempt_read will throw
        // an error so unwrap() is safe here.
        let value_len = u32::from_be_bytes(buf[0..4].try_into().unwrap());
        attempt_read(handle, buf, value_len as usize)?;
        let value = str::from_utf8(&buf[0..value_len as usize])
            .context(types::KeyValueInvalidUtf8)?
            .to_string();

        Ok(Self::Set { key, value })
    }

    pub fn key(&self) -> String {
        match self {
            Self::Set { key, value } => key.clone(),
            Self::Remove { key } => key.clone(),
        }
    }

    pub fn value(&self) -> Option<String> {
        if let Self::Set { key, value } = self {
            return Some(value.clone());
        }

        None
    }

    pub fn len_bytes(&self) -> u64 {
        match self {
            Self::Set { key, value } => 7 + key.len() as u64 + value.len() as u64,
            Self::Remove { key } => 3 + key.len() as u64,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{kv_store::command::Command, types};
    use snafu::ResultExt;
    use std::{
        self, fs,
        io::{self, prelude::*},
    };
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn ser_de_set_variant() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();
        let dir = open_tempdir();
        let file_path = dir.path().join("thing.txt");

        let comm = Command::Set {
            key: test_key.clone(),
            value: test_value.clone(),
        };
        let file = fs::File::create(file_path.clone())
            .context(types::OpenFile { file_name: file_path.to_string_lossy().clone().to_string() })?;
        let mut writer = io::BufWriter::new(file);
        comm.serialize(&mut writer)?;
        drop(writer);

        let mut buf: Vec<u8> = vec![0; 10];
        let mut file = fs::File::open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().clone().to_string() })?;
        let reader = &mut io::BufReader::new(&mut file as &mut dyn io::Read) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let comm_de = Command::deserialize(&mut handle, &mut buf)?;

        if let Command::Set { key, value } = comm_de {
            std::assert_eq!(key, test_key);
            std::assert_eq!(value, test_value);

            return Ok(());
        }

        panic!("Deserialized command not Command::Set variant");
    }

    #[test]
    fn ser_de_remove_variant() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let dir = open_tempdir();
        let file_path = dir.path().join("thing.txt");

        let comm = Command::Remove {
            key: test_key.clone(),
        };
        let file = fs::File::create(file_path.clone())
            .context(types::OpenFile { file_name: file_path.to_string_lossy().clone().to_string() })?;
        let mut writer = io::BufWriter::new(file);
        comm.serialize(&mut writer)?;
        drop(writer);

        let mut buf: Vec<u8> = vec![0; 10];
        let mut file = fs::File::open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().clone().to_string() })?;
        let reader = &mut io::BufReader::new(&mut file as &mut dyn io::Read) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let comm_de = Command::deserialize(&mut handle, &mut buf)?;

        if let Command::Remove { key } = comm_de {
            std::assert_eq!(key, test_key);

            return Ok(());
        }

        panic!("Deserialized command not Command::Remove variant");
    }

    #[test]
    fn bytes_len() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();

        let comm = Command::Set {
            key: test_key.clone(),
            value: test_value.clone(),
        };

        let mut buf: Vec<u8> = Vec::new();
        comm.serialize(&mut buf)?;

        std::assert_eq!(buf.len() as u64, comm.len_bytes());

        let comm = Command::Remove {
            key: test_key.clone(),
        };

        let mut buf: Vec<u8> = Vec::new();
        comm.serialize(&mut buf)?;

        std::assert_eq!(buf.len() as u64, comm.len_bytes());

        Ok(())
    }
}
