use crate::{
    constants,
    kv_store::{
        index_file_page::IndexFilePage,
        log_pointer::{LogPointer, LOG_POINTER_METADATA_BYTES},
        page_bytes_reserved_file::{
            PageBytesReservedFile,
            get_page_reserved_bytes_file_path,
        },
        log_file::LogFileReader,
        utils,
    },
    types,
};
use snafu::ResultExt;
use std::{convert::TryInto, fs, path};

pub fn get_index_file_path(dir_path: &path::Path) -> path::PathBuf {
    dir_path.join(constants::file_name::INDEX)
}

pub struct IndexFile<'a> {
    dir_path: &'a path::Path,
}
impl<'a> IndexFile<'a> {
    pub fn new(dir_path: &'a path::Path) -> Self {
        IndexFile { dir_path }
    }

    pub fn generate(&self) -> types::Result {
        let page_len = utils::get_page_len_bytes()?;
        let mut log_file_names = utils::get_log_file_names(self.dir_path)?;

        let index_file_path = self.get_path();
        if index_file_path.exists() {
            fs::remove_file(&index_file_path)
                .context(types::OpenFile { file_name: index_file_path.to_string_lossy().to_string() })?;
        }

        let page_bytes_reserved_file_path = get_page_reserved_bytes_file_path(&self.dir_path);
        if page_bytes_reserved_file_path.exists() {
            fs::remove_file(&page_bytes_reserved_file_path)
                .context(types::OpenFile { file_name: page_bytes_reserved_file_path.to_string_lossy().to_string() })?;
        }

        let mut pointers_buf: Vec<LogPointer> = Vec::new();
        let mut pointer_buf_bytes = 0;

        for log_file_name in log_file_names.drain(..) {
            let file_path = self.dir_path.join(log_file_name);
            let mut log_file_reader = LogFileReader::new(file_path.as_path())?;

            for log_pointer in log_file_reader.generate_command_pointers()?.drain(..) {
                let next_pointer_bytes = log_pointer.len_bytes();

                // Flush bytes if page capacity met.
                if pointer_buf_bytes + next_pointer_bytes > page_len {
                    let page = self.create_page()?;
                    page.write_pointers(&pointers_buf)?;

                    pointers_buf.clear();
                    pointer_buf_bytes = 0;
                }

                pointers_buf.push(log_pointer);
                pointer_buf_bytes += next_pointer_bytes;
            }
        }

        // Regenerate page bytes reserved file.
        let page_bytes_reserved_file = PageBytesReservedFile::read_or_init(self.dir_path)?;
        page_bytes_reserved_file.write()?;

        Ok(())
    }

    fn open_file(&self) -> types::Result<fs::File> {
        let index_file_path = self.get_path();

        fs::OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(&index_file_path)
            .context(types::OpenFile { file_name: index_file_path.to_string_lossy().to_string() })
    }

    pub fn num_pages(&self) -> types::Result<usize> {
        let index_file = self.open_file()?;
        let page_len = utils::get_page_len_bytes()?;

        let total_pages = index_file
            .metadata()
            .expect("Unable to read index file metadata")
            .len() as usize
            / page_len;

        Ok(total_pages)
    }

    pub fn get_page(&self, page_num: usize) -> types::Result<IndexFilePage> {
        let page_len = utils::get_page_len_bytes()?;

        Ok(IndexFilePage::new(self.dir_path, page_num, page_len))
    }

    pub fn create_page(&self) -> types::Result<IndexFilePage> {
        let next_page_num = self.num_pages()?;
        self.get_page(next_page_num)
    }

    fn get_path(&self) -> path::PathBuf {
        get_index_file_path(self.dir_path)
    }

    // Returns page number, log pointer index.
    pub fn query(&self, needle: &str) -> types::Result<Option<(usize, usize)>> {
        let page_len = utils::get_page_len_bytes()?;
        let total_pages = self.num_pages()?;
        let needle = needle.as_bytes();

        let mut buf = Vec::new();
        for page_num in 0..total_pages {
            let page = self.get_page(page_num)?;
            let num_pointers = page.num_pointers()?;
            page.read(&mut buf, page_len)?;

            let mut i = 8usize;
            for pointer_index in 0..num_pointers {
                i += LOG_POINTER_METADATA_BYTES;
                let key_len = u32::from_be_bytes(buf[i - 4..i].try_into().unwrap()) as usize;

                if key_len != needle.len() {
                    i += key_len;
                    continue;
                }

                if needle[..] == buf[i..i + key_len] {
                    return Ok(Some((page_num, pointer_index as usize)));
                }

                i += key_len;
            }

            buf.clear();
        }

        Ok(None)
    }

    pub fn upsert(&self, log_pointer: LogPointer) -> types::Result<()> {
        let page_len = utils::get_page_len_bytes()?;
        let query_res = self.query(&log_pointer.key)?;

        let mut page_bytes_reserved_file = PageBytesReservedFile::read_or_init(self.dir_path)?;
        let log_pointer_bytes = log_pointer.len_bytes();

        if let Some((page_index, pointer_index)) = query_res {
            let page = self.get_page(page_index)?;
            let mut pointers = page.read_pointers()?;
            pointers[pointer_index] = log_pointer;
            page.write_pointers(&pointers)?;

            return Ok(());
        }

        if !page_bytes_reserved_file.page_reserved_bytes_map.is_empty() {
            let mut target = (&0, page_bytes_reserved_file.get_page_bytes(0).unwrap());
            for candidate in page_bytes_reserved_file.page_reserved_bytes_map.iter() {
                if *candidate.1 < *target.1 {
                    target = candidate;
                }
            }

            let capacity = page_len - *target.1 - 8;
            if log_pointer_bytes < capacity {
                let page = self.get_page(*target.0)?;
                let mut pointers = page.read_pointers()?;

                pointers.push(log_pointer);
                page.write_pointers(&pointers)?;

                page_bytes_reserved_file.add_page_bytes(*target.0, log_pointer_bytes as i64)?;
                page_bytes_reserved_file.write()?;

                return Ok(());
            }
        }

        let page = self.create_page()?;
        let pointers = vec![log_pointer];
        page.write_pointers(&pointers)?;

        page_bytes_reserved_file.set_page_bytes(page.page_num, log_pointer_bytes);
        page_bytes_reserved_file.write()?;

        Ok(())
    }

    pub fn delete(&self, key: &str) -> types::Result<Option<LogPointer>> {
        let query_res = self.query(key)?;

        if query_res.is_none() {
            return Ok(None);
        }

        let (page_index, pointer_index) = query_res.unwrap();

        let page = self.get_page(page_index)?;
        let mut pointers = page.read_pointers()?;
        let deleted = pointers.swap_remove(pointer_index);
        page.write_pointers(&pointers)?;

        let deleted_bytes = deleted.len_bytes();
        let mut page_bytes_reserved_file = PageBytesReservedFile::read_or_init(self.dir_path)?;
        let reserved_bytes = page_bytes_reserved_file.get_page_bytes(page_index).unwrap();
        page_bytes_reserved_file.set_page_bytes(page_index, reserved_bytes - deleted_bytes);
        page_bytes_reserved_file.write()?;

        Ok(Some(deleted))
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        kv_store::{
            index_file::IndexFile,
            log_files::LogFiles,
            log_pointer::LogPointer,
        },
        types,
    };
    use std;
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn query_1_page() -> types::Result<()> {
        let dir = open_tempdir();
        let index_file = IndexFile::new(&dir.path());
        let test_log_file_name = LogFiles::generate_log_file_name();

        let mut test_pointers: Vec<LogPointer> = Vec::new();
        for i in 0..5 {
            let next_pointer =
                LogPointer::new(i, test_log_file_name.clone(), format!("test_key_{}", i));
            test_pointers.push(next_pointer);
        }

        let page = index_file.get_page(0)?;
        page.write_pointers(&test_pointers)?;

        let res = index_file.query("test_key_0")?;
        std::assert!(res.is_some());
        let res = res.unwrap();
        std::assert_eq!(res, (0, 0));

        let res = index_file.query("test_key_2")?;
        std::assert!(res.is_some());
        let res = res.unwrap();
        std::assert_eq!(res, (0, 2));

        let res = index_file.query("test_key_6")?;
        std::assert!(res.is_none());

        Ok(())
    }

    #[test]
    fn query_5_page() -> types::Result<()> {
        let dir = open_tempdir();
        let index_file = IndexFile::new(&dir.path());
        let test_log_file_name = LogFiles::generate_log_file_name();

        for page_index in 0..5 {
            let mut test_pointers: Vec<LogPointer> = Vec::new();
            for i in 0..5 {
                let next_pointer = LogPointer::new(
                    i,
                    test_log_file_name.clone(),
                    format!("test_key_{}_{}", page_index, i),
                );
                test_pointers.push(next_pointer);
            }

            let page = index_file.get_page(page_index)?;
            page.write_pointers(&test_pointers)?;
        }

        let res = index_file.query("test_key_0_0")?;
        std::assert!(res.is_some());
        let res = res.unwrap();
        std::assert_eq!(res, (0, 0));

        let res = index_file.query("test_key_2_2")?;
        std::assert!(res.is_some());
        let res = res.unwrap();
        std::assert_eq!(res, (2, 2));

        let res = index_file.query("test_key_4_3")?;
        std::assert!(res.is_some());
        let res = res.unwrap();
        std::assert_eq!(res, (4, 3));

        let res = index_file.query("test_key_6_3")?;
        std::assert!(res.is_none());

        Ok(())
    }

    #[test]
    fn upsert_empty() -> types::Result<()> {
        let dir = open_tempdir();
        let index_file = IndexFile::new(&dir.path());
        let test_log_file_name = LogFiles::generate_log_file_name();
        let test_key = "test_key";

        let test_pointer = LogPointer::new(0, test_log_file_name.clone(), test_key.to_string());

        index_file.upsert(test_pointer)?;

        let page = index_file.get_page(0)?;
        let pointers = page.read_pointers()?;

        std::assert!(pointers.len() == 1);
        std::assert_eq!(pointers[0].key, test_key.to_string());

        Ok(())
    }

    #[test]
    fn upsert_page_1() -> types::Result<()> {
        let dir = open_tempdir();
        let index_file = IndexFile::new(&dir.path());
        let test_log_file_name = LogFiles::generate_log_file_name();

        let pointers: Vec<LogPointer> = vec![LogPointer::new(
            0,
            test_log_file_name.clone(),
            "test_key".to_string(),
        )];

        let page = index_file.get_page(0)?;
        page.write_pointers(&pointers)?;

        let target_key = "target_key";
        let test_pointer = LogPointer::new(0, test_log_file_name.clone(), target_key.to_string());

        index_file.upsert(test_pointer)?;

        let page = index_file.get_page(0)?;
        let pointers = page.read_pointers()?;

        std::assert!(pointers.len() == 2);
        std::assert_eq!(pointers[1].key, target_key.to_string());

        Ok(())
    }

    #[test]
    fn upsert_page_2() -> types::Result<()> {
        let dir = open_tempdir();
        let index_file = IndexFile::new(&dir.path());
        let test_log_file_name = LogFiles::generate_log_file_name();
        let test_key = "test_key";

        let mut num_bytes = 0;
        let mut pointers: Vec<LogPointer> = Vec::new();
        loop {
            let next_pointer = LogPointer::new(0, test_log_file_name.clone(), test_key.to_string());
            let next_bytes = next_pointer.len_bytes();

            if next_bytes + num_bytes > 1000 {
                break;
            }

            pointers.push(next_pointer);
            num_bytes += next_bytes;
        }

        let page = index_file.get_page(0)?;
        page.write_pointers(&pointers)?;

        let target_key = "target_key";
        let test_pointer = LogPointer::new(0, test_log_file_name.clone(), target_key.to_string());

        index_file.upsert(test_pointer)?;

        let page = index_file.get_page(1)?;
        let pointers = page.read_pointers()?;

        std::assert!(pointers.len() == 1);
        std::assert_eq!(pointers[0].key, target_key.to_string());

        Ok(())
    }

    #[test]
    fn upsert_replace() -> types::Result<()> {
        let dir = open_tempdir();
        let index_file = IndexFile::new(&dir.path());
        let test_log_file_name = LogFiles::generate_log_file_name();
        let target_key = "target_key";

        let pointers: Vec<LogPointer> = vec![LogPointer::new(
            0,
            test_log_file_name.clone(),
            target_key.to_string(),
        )];

        let page = index_file.get_page(0)?;
        page.write_pointers(&pointers)?;

        let test_pointer = LogPointer::new(0, test_log_file_name.clone(), target_key.to_string());

        index_file.upsert(test_pointer)?;

        let page = index_file.get_page(0)?;
        let pointers = page.read_pointers()?;

        std::assert!(pointers.len() == 1);
        std::assert_eq!(pointers[0].key, target_key.to_string());

        Ok(())
    }
}
