use crate::kv_store::{index_file::IndexFile, index_file_page::IndexFilePage, types};
use std::iter;

pub struct IndexFileIter<'a> {
    index_file: &'a IndexFile<'a>,
    page_index: usize,
    total_pages: usize,
}
impl<'a> IndexFileIter<'a> {
    pub fn new(index_file: &'a IndexFile<'a>, total_pages: usize) -> Self {
        Self {
            index_file,
            page_index: 0,
            total_pages,
        }
    }
}
impl<'a> iter::Iterator for IndexFileIter<'a> {
    type Item = types::Result<IndexFilePage<'a>>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.page_index >= self.total_pages {
            return None;
        }

        let next = self.index_file.get_page(self.page_index);
        self.page_index += 1;
        Some(next)
    }
}
