use super::{index_file::get_index_file_path, log_pointer::LogPointer, types};
use snafu::ResultExt;
use std::{
    convert::TryInto,
    fs,
    io::{self, prelude::*},
    path,
};

#[derive(Debug)]
pub struct IndexFilePage<'a> {
    dir_path: &'a path::Path,
    pub page_num: usize,
    page_len: usize,
}
impl<'a> IndexFilePage<'a> {
    pub fn new(dir_path: &'a path::Path, page_num: usize, page_len: usize) -> Self {
        IndexFilePage {
            dir_path,
            page_num,
            page_len,
        }
    }

    fn open_read(&self) -> types::Result<fs::File> {
        let index_file_path = get_index_file_path(&self.dir_path);

        fs::OpenOptions::new()
            .read(true)
            .open(&index_file_path)
            .context(types::OpenFile { file_name: index_file_path.to_string_lossy().to_string() })
    }

    fn open_write(&self) -> types::Result<fs::File> {
        let index_file_path = get_index_file_path(&self.dir_path);

        fs::OpenOptions::new()
            .write(true)
            .truncate(false)
            .create(true)
            .open(&index_file_path)
            .context(types::OpenFile { file_name: index_file_path.to_string_lossy().to_string() })
    }

    pub fn read(&self, buf: &mut Vec<u8>, page_len: usize) -> types::Result<()> {
        let mut file = self.open_read()?;
        file.seek(io::SeekFrom::Start((self.page_num * self.page_len) as u64))
            .context(types::Reading)?;
        let reader = io::BufReader::new(file);
        let mut handle = reader.take(page_len as u64);

        handle.read_to_end(buf).context(types::Reading)?;

        Ok(())
    }

    pub fn num_pointers(&self) -> types::Result<u64> {
        let mut file = self.open_read()?;
        file.seek(io::SeekFrom::Start((self.page_num * self.page_len) as u64))
            .context(types::Reading)?;
        let mut buf = vec![0u8; 8];
        if let Err(err) = file.read_exact(&mut buf) {
            match err.kind() {
                io::ErrorKind::UnexpectedEof => {
                    return Ok(0);
                }
                _ => return Err(err).context(types::Reading),
            }
        }

        let num_pointers = u64::from_be_bytes(buf[0..8].try_into().unwrap());
        Ok(num_pointers)
    }

    pub fn read_pointers(&self) -> types::Result<Vec<LogPointer>> {
        let num_pointers = self.num_pointers()?;
        if num_pointers == 0 {
            return Ok(Vec::new());
        }

        let mut buf = vec![0u8; 8];
        let mut file = self.open_read()?;
        file.seek(io::SeekFrom::Start((self.page_num * self.page_len) as u64))
            .context(types::Reading)?;

        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        reader.read_exact(&mut buf).context(types::Reading)?;
        let mut handle = reader.take(0);

        let mut pointers: Vec<LogPointer> = Vec::new();
        for _ in 0..num_pointers {
            let next_pointer = LogPointer::deserialize(&mut handle, &mut buf)?.0;
            pointers.push(next_pointer);
        }

        Ok(pointers)
    }

    pub fn write_pointers(&self, log_pointers: &[LogPointer]) -> types::Result<()> {
        let total_pointer_bytes = log_pointers
            .iter()
            .fold(0usize, |acc, curr| acc + curr.len_bytes());

        if total_pointer_bytes + 8 > self.page_len {
            return types::WriteOverflow {}.fail();
        }

        let mut buf = (log_pointers.len() as u64).to_be_bytes().to_vec();
        for log_pointer in log_pointers.iter() {
            log_pointer.serialize(&mut buf)?;
        }

        let num_padding_bytes = self.page_len - buf.len();
        for _ in 0..num_padding_bytes {
            buf.push(0u8);
        }

        let mut file = self.open_write()?;
        file.seek(io::SeekFrom::Start((self.page_num * self.page_len) as u64))
            .context(types::Reading)?;
        file.write_all(&buf).context(types::Writing)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        kv_store::{index_file_page::IndexFilePage, log_files::LogFiles, log_pointer::LogPointer},
        types,
    };
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn write_read_log_pointers() -> types::Result<()> {
        let dir = open_tempdir();
        let test_log_file_name = LogFiles::generate_log_file_name();

        let mut test_pointers: Vec<LogPointer> = Vec::new();
        for i in 0..5 {
            let next_pointer =
                LogPointer::new(i, test_log_file_name.clone(), format!("test_key_{}", i));
            test_pointers.push(next_pointer);
        }

        let index_file_page = IndexFilePage::new(&dir.path(), 0, 1024);
        index_file_page.write_pointers(&test_pointers)?;

        let read_pointers = index_file_page.read_pointers()?;
        for (read, test) in read_pointers.iter().zip(test_pointers.iter()) {
            std::assert_eq!(read, test);
        }

        Ok(())
    }

    #[test]
    fn write_read_log_pointers_page_2() -> types::Result<()> {
        let dir = open_tempdir();
        let test_log_file_name = LogFiles::generate_log_file_name();

        let mut test_pointers: Vec<LogPointer> = Vec::new();
        for i in 0..5 {
            let next_pointer =
                LogPointer::new(i, test_log_file_name.clone(), format!("test_key_{}", i));
            test_pointers.push(next_pointer);
        }

        let index_file_page = IndexFilePage::new(&dir.path(), 0, 1024);
        index_file_page.write_pointers(&test_pointers)?;

        let mut test_pointers: Vec<LogPointer> = Vec::new();
        for i in 5..10 {
            let next_pointer =
                LogPointer::new(i, test_log_file_name.clone(), format!("test_key_{}", i));
            test_pointers.push(next_pointer);
        }

        let index_file_page = IndexFilePage::new(&dir.path(), 1, 1024);
        index_file_page.write_pointers(&test_pointers)?;

        let read_pointers = index_file_page.read_pointers()?;
        for (read, test) in read_pointers.iter().zip(test_pointers.iter()) {
            std::assert_eq!(read, test);
        }

        Ok(())
    }
}
