use crate::{
    kv_store::{command::Command, index_file::IndexFile, log_files::LogFiles},
    types,
};
use snafu::ResultExt;
use std::path;

/// A log-structured implementation of a key-value store.
#[derive(Clone)]
pub struct KvStore {
    dir_path: String,
}
impl KvStore {
    /// Opens a KvStore instance for the specified path.
    //
    // # Errors
    // Will return an error if the specified path is invalid, or not a directory.
    pub fn open(dir_path: &path::Path) -> types::Result<Self> {
        let dir_path = dir_path.canonicalize().context(types::InvalidWorkingDir)?;

        if !dir_path.is_dir() {
            return types::WorkingDirNotDir {}.fail();
        }

        let kv_store = KvStore {
            dir_path: dir_path.to_str().unwrap().to_string(),
        };

        Ok(kv_store)
    }

    /// Keys a value for the specified key. None if unset.
    //
    // # Errors
    // Returns an error if unable to read the index file.
    pub fn get(&self, key: &str) -> types::Result<Option<String>> {
        let dir_path = path::Path::new(&self.dir_path);
        let index_file = IndexFile::new(&dir_path);

        let query_res = index_file.query(key)?;
        if query_res.is_none() {
            return Ok(None);
        }
        let (target_page, target_index) = query_res.unwrap();

        let page = index_file.get_page(target_page)?;
        let pointers = page.read_pointers()?;

        let log_pointer = pointers.get(target_index);
        if log_pointer.is_none() {
            return Ok(None);
        }
        let log_pointer = log_pointer.unwrap();
        let command: Option<Command> = LogFiles::read_command(&dir_path, &log_pointer)?;
        if command.is_none() {
            return Ok(None);
        }
        let command = command.unwrap();

        Ok(command.value())
    }

    /// Sets a key-value pair.
    //
    // # Errors
    // Returns an error if unable to append command to the active log file, or if unable to update
    // the index file.
    pub fn set(&self, key: String, value: String) -> types::Result<()> {
        let dir_path = path::Path::new(&self.dir_path);
        let command = Command::Set { key, value };

        LogFiles::append_command(dir_path, &command)?;

        Ok(())
    }

    /// Removes a key-value pair.
    //
    // # Errors
    // Returns an error if unable to append command to the active log file, or if unable to update
    // the index file.
    pub fn remove(&self, key: String) -> types::Result<()> {
        let dir_path = path::Path::new(&self.dir_path);
        let command = Command::Remove { key };

        LogFiles::append_command(dir_path, &command)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::{kv_store::KvStore, types::Result};
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn set_get_1() -> Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();
        let dir = open_tempdir();

        let kv_store = KvStore::open(dir.path())?;
        kv_store.set(test_key.clone(), test_value.clone())?;
        let result = kv_store.get(&test_key)?;

        std::assert!(result.is_some());

        let result = result.unwrap();
        std::assert_eq!(result, test_value);

        Ok(())
    }

    #[test]
    fn set_get_2() -> Result<()> {
        let dir = open_tempdir();
        let kv_store = KvStore::open(dir.path())?;

        for i in 0..2 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            kv_store.set(test_key, test_value)?;
        }

        for i in 0..2 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            let result = kv_store.get(&test_key)?;
            std::assert!(result.is_some());
            let result = result.unwrap();
            std::assert_eq!(result, test_value);
        }

        Ok(())
    }

    #[test]
    fn set_get_100() -> Result<()> {
        let dir = open_tempdir();
        let kv_store = KvStore::open(dir.path())?;

        for i in 0..100 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            kv_store.set(test_key, test_value)?;
        }

        for i in 0..100 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            let result = kv_store.get(&test_key)?;
            std::assert!(result.is_some());
            let result = result.unwrap();
            std::assert_eq!(result, test_value);
        }

        Ok(())
    }

    #[test]
    fn set_get_1000() -> Result<()> {
        let dir = open_tempdir();
        let kv_store = KvStore::open(dir.path())?;

        for i in 0..1000 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            kv_store.set(test_key, test_value)?;
        }

        for i in 0..1000 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            let result = kv_store.get(&test_key)?;
            std::assert!(result.is_some());
            let result = result.unwrap();
            std::assert_eq!(result, test_value);
        }

        Ok(())
    }

    #[test]
    fn set_get_1000_replace_500() -> Result<()> {
        let dir = open_tempdir();
        let kv_store = KvStore::open(dir.path())?;

        for i in 0..1000 {
            let test_key = format!("test_key_{}", i % 500);
            let test_value = format!("test_value_{}", i % 500);

            kv_store.set(test_key, test_value)?;
        }

        for i in 0..1000 {
            let test_key = format!("test_key_{}", i % 500);
            let test_value = format!("test_value_{}", i % 500);

            let result = kv_store.get(&test_key)?;
            std::assert!(result.is_some());
            let result = result.unwrap();
            std::assert_eq!(result, test_value);
        }

        Ok(())
    }

    #[test]
    fn set_delete_get_1() -> Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();
        let dir = open_tempdir();

        let kv_store = KvStore::open(dir.path())?;
        kv_store.set(test_key.clone(), test_value.clone())?;
        kv_store.remove(test_key.clone())?;
        let result = kv_store.get(&test_key)?;

        std::assert!(result.is_none());

        Ok(())
    }

    #[test]
    fn set_delete_even_get_500() -> Result<()> {
        let dir = open_tempdir();
        let kv_store = KvStore::open(dir.path())?;

        for i in 0..500 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            kv_store.set(test_key.clone(), test_value)?;

            if i % 2 == 0 {
                kv_store.remove(test_key)?;
            }
        }

        for i in 0..500 {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            let result = kv_store.get(&test_key)?;

            if i % 2 == 0 {
                std::assert!(result.is_none());
            } else {
                std::assert!(result.is_some());
                let result = result.unwrap();
                std::assert_eq!(result, test_value);
            }
        }

        Ok(())
    }
}
