use std::{path, fs, ops, io::{self, prelude::*}};
use super::{
    command::Command,
    log_pointer::LogPointer,
    types,
};

use snafu::ResultExt;
use uuid::Uuid;

// TODO: Create an iterator so these can be lazy-loaded
pub struct LogFileReader<'a> {
    file_path: &'a path::Path,
    reader: io::BufReader<fs::File>,
}
impl<'a> LogFileReader<'a> {
    pub fn new(file_path: &'a path::Path) -> types::Result<Self> {
        let file = fs::OpenOptions::new()
            .read(true)
            .open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().to_string() })?;
        let reader = io::BufReader::new(file);

        Ok(Self { reader, file_path })
    }

    pub fn read_command(&mut self, log_pointer: &LogPointer) -> types::Result<Command> {
        self.reader.seek(io::SeekFrom::Start(log_pointer.byte_offset))
            .context(types::Reading)?;
        let mut buf: Vec<u8> = Vec::new();
        let reader_ref = &mut self.reader as &mut dyn io::BufRead;

        Command::deserialize(&mut reader_ref.take(0), &mut buf)
    }

    pub fn generate_command_pointers(&mut self) -> types::Result<Vec<LogPointer>> {
        self.reader.seek(io::SeekFrom::Start(0)).context(types::Reading)?;
        let mut pointers: Vec<LogPointer> = Vec::new();
        let log_file_name = self.file_path
            .file_name()
            .ok_or(io::Error::new(io::ErrorKind::Other, "Unable to read log fil name."))
            .context(types::Reading)?
            .to_string_lossy()
            .to_string();
        let mut buf = vec![0u8; 256];

        let reader_ref = &mut self.reader as &mut dyn io::BufRead;
        let mut handle = reader_ref.take(0);

        let mut byte_offset = 0;
        while let Ok(command) = Command::deserialize(&mut handle, &mut buf) {
            let pointer = LogPointer::new(byte_offset, log_file_name.clone(), command.key());
            pointers.push(pointer);
            byte_offset += command.len_bytes() as u64;
        }

        Ok(pointers)
    }
}

pub struct LogFile {
    dir_path: path::PathBuf,
    name: Uuid,
    commands: Vec<Command>,
    delta: Vec<Command>,
}
impl LogFile {
    pub fn new(dir_path: path::PathBuf, name: Uuid, commands: Vec<Command>) -> Self {
        Self { dir_path, name, commands, delta: Vec::new() }
    }
    pub fn flush(&self) -> types::Result {
        todo!();
    }
}
impl ops::Drop for LogFile {
    fn drop(&mut self) {
        self.flush().expect("Unable to flush log file");
    }
}
