use uuid::Uuid;
use lru::LruCache;
use snafu::ResultExt;
use crate::{
    kv_store::{
        log_file::LogFile,
        command::Command,
    },
    types,
};
use std::{path, fs, io::{self, prelude::*}};

pub struct LogFileVendor {
    dir_path: path::PathBuf,
    cache: LruCache<Uuid, LogFile>,
}
impl LogFileVendor {
    pub fn new(dir_path: path::PathBuf, capacity: usize) -> Self {
        Self {
            dir_path,
            cache: LruCache::new(capacity),
        }
    }

    fn read_log_file(&self, name: &Uuid) -> types::Result<Option<LogFile>> {
        let file_path = self.dir_path.join(name.to_string());
        if !file_path.exists() {
            return Ok(None);
        }
        let file = fs::File::open(&file_path)
            .context(types::OpenFile { file_name: name.to_string() })?;
        let mut reader = io::BufReader::new(file);
        let reader_ref = &mut reader as &mut dyn io::BufRead;
        let mut handle = reader_ref.take(0);
        let mut buf = vec![0u8; 512];

        let mut commands: Vec<Command> = Vec::new();
        while let Ok(command) = Command::deserialize(&mut handle, &mut buf) {
            commands.push(command);
        }

        Ok(Some(LogFile::new(file_path, name.clone(), commands)))
    }

    pub fn get(&mut self, name: Uuid) -> types::Result<Option<&LogFile>> {
        if !self.cache.contains(&name) {
            let log_file = self.read_log_file(&name)?;
            if log_file.is_none() {
                return Ok(None);
            }
            let log_file = log_file.unwrap();

            self.cache.put(name, log_file);
        }

        Ok(self.cache.get(&name))
    }

    pub fn get_mut(&mut self, name: Uuid) -> types::Result<Option<&mut LogFile>> {
        if !self.cache.contains(&name) {
            let log_file = self.read_log_file(&name)?;
            if log_file.is_none() {
                return Ok(None);
            }
            let log_file = log_file.unwrap();

            self.cache.put(name, log_file);
        }

        Ok(self.cache.get_mut(&name))
    }

    pub fn create(&mut self) -> Uuid {
        let next_name = Uuid::new_v4();
        let next_log_file = LogFile::new(self.dir_path.clone(), next_name, Vec::new());
        self.cache.put(next_name, next_log_file);
        next_name
    }
}

// TODO: Testing
