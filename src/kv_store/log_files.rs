use crate::{
    constants,
    kv_store::{
        command::Command,
        index_file::IndexFile,
        log_pointer::LogPointer,
        working_state::WorkingState,
        log_file::LogFileReader,
        utils,
    },
    types,
};
use snafu::ResultExt;
use std::{fs, io, path};
use uuid::Uuid;

pub struct LogFiles;
impl LogFiles {
    pub fn generate_log_file_name() -> String {
        let hash = Uuid::new_v4().to_string();
        format!("{}{}", hash, constants::LOG_EXTENSION)
    }

    pub fn append_command(dir_path: &path::Path, command: &Command) -> types::Result<LogPointer> {
        let mut working_state = WorkingState::read_or_init(dir_path)?;
        let mut file_path = dir_path.join(working_state.active_log_file_path.clone());
        let file_reserved_bytes = file_path.metadata().map(|m| m.len()).unwrap_or(0);
        if file_reserved_bytes + command.len_bytes() > utils::get_log_file_len_bytes()? as u64 {
            working_state = WorkingState::generate_new(working_state.stale_bytes);
            file_path = dir_path.join(working_state.active_log_file_path.clone());
        }

        let file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(false)
            .append(true)
            .open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().to_string() })?;

        let pointer_offset = file.metadata().context(types::Reading)?.len();
        let next_pointer = LogPointer::new(
            pointer_offset,
            working_state.active_log_file_path.clone(),
            command.key(),
        );

        let mut writer = io::BufWriter::new(file);
        command.serialize(&mut writer)?;

        let index_file = IndexFile::new(&dir_path);
        match command {
            Command::Set { key: _, value: _ } => {
                index_file.upsert(next_pointer.clone())?;
            }
            Command::Remove { key } => {
                let deleted = index_file.delete(&key)?;
                if let Some(deleted) = deleted {
                    let deleted_bytes = deleted.len_bytes();
                    working_state.stale_bytes += deleted_bytes as isize;
                }
            }
        };

        working_state.write_state_file(&dir_path)?;

        Ok(next_pointer)
    }

    pub fn read_command(
        dir_path: &path::Path,
        log_pointer: &LogPointer,
    ) -> types::Result<Option<Command>> {
        let file_path = dir_path.join(log_pointer.file_name.clone());

        let mut log_file_reader = LogFileReader::new(&file_path)?;
        log_file_reader.read_command(log_pointer).map(Some)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        kv_store::{command::Command, log_files::LogFiles, working_state::WorkingState},
        types,
    };
    use std::{
        fs,
        io::{self, prelude::*},
    };
    use tempfile;

    #[test]
    fn append_set_command() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();
        let dir = tempfile::tempdir().unwrap();

        let comm = Command::Set {
            key: test_key.clone(),
            value: test_value.clone(),
        };

        LogFiles::append_command(dir.path(), &comm)?;

        let state = WorkingState::read_state_file(dir.path())?;
        let log_path = dir.path().join(state.active_log_file_path);

        let mut buf: Vec<u8> = vec![0; 10];
        let mut file = fs::File::open(&log_path).unwrap();
        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let comm_de = Command::deserialize(&mut handle, &mut buf)?;

        if let Command::Set { key, value } = comm_de {
            std::assert_eq!(key, test_key);
            std::assert_eq!(value, test_value);

            return Ok(());
        }

        panic!("Deserialized command not Command::Set variant");
    }

    #[test]
    fn append_remove_command() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let dir = tempfile::tempdir().unwrap();

        let comm = Command::Remove {
            key: test_key.clone(),
        };

        LogFiles::append_command(dir.path(), &comm)?;

        let state = WorkingState::read_state_file(dir.path())?;
        let log_path = dir.path().join(state.active_log_file_path);

        let mut buf: Vec<u8> = vec![0; 10];
        let mut file = fs::File::open(&log_path).unwrap();
        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let comm_de = Command::deserialize(&mut handle, &mut buf)?;

        if let Command::Remove { key } = comm_de {
            std::assert_eq!(key, test_key);

            return Ok(());
        }

        panic!("Deserialized command not Command::Remove variant");
    }

    #[test]
    fn append_set_remove_command() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();
        let dir = tempfile::tempdir().unwrap();

        let comm_set = Command::Set {
            key: test_key.clone(),
            value: test_value.clone(),
        };
        let comm_remove = Command::Remove {
            key: test_key.clone(),
        };

        LogFiles::append_command(dir.path(), &comm_set)?;
        LogFiles::append_command(dir.path(), &comm_remove)?;

        let state = WorkingState::read_state_file(dir.path())?;
        let log_path = dir.path().join(state.active_log_file_path);

        let mut buf: Vec<u8> = vec![0, 20];
        let mut file = fs::File::open(&log_path).unwrap();
        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let comm_de_set = Command::deserialize(&mut handle, &mut buf)?;
        let comm_de_remove = Command::deserialize(&mut handle, &mut buf)?;

        if let Command::Set { key, value } = comm_de_set {
            std::assert_eq!(key, test_key);
            std::assert_eq!(value, test_value);
        } else {
            panic!("Deserialized command not Command::Set variant");
        }
        if let Command::Remove { key } = comm_de_remove {
            std::assert_eq!(key, test_key);
        } else {
            panic!("Deserialized command not Command::Remove variant");
        }

        Ok(())
    }

    #[test]
    fn append_remove_set_command() -> types::Result<()> {
        let test_key = "test_key".to_string();
        let test_value = "test_value".to_string();
        let dir = tempfile::tempdir().unwrap();

        let comm_set = Command::Set {
            key: test_key.clone(),
            value: test_value.clone(),
        };
        let comm_remove = Command::Remove {
            key: test_key.clone(),
        };

        LogFiles::append_command(dir.path(), &comm_remove)?;
        LogFiles::append_command(dir.path(), &comm_set)?;

        let state = WorkingState::read_state_file(dir.path())?;
        let log_path = dir.path().join(state.active_log_file_path);

        let mut buf: Vec<u8> = vec![0, 20];
        let mut file = fs::File::open(&log_path).unwrap();
        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let comm_de_remove = Command::deserialize(&mut handle, &mut buf)?;
        let comm_de_set = Command::deserialize(&mut handle, &mut buf)?;

        if let Command::Set { key, value } = comm_de_set {
            std::assert_eq!(key, test_key);
            std::assert_eq!(value, test_value);
        } else {
            panic!("Deserialized command not Command::Set variant");
        }
        if let Command::Remove { key } = comm_de_remove {
            std::assert_eq!(key, test_key);
        } else {
            panic!("Deserialized command not Command::Remove variant");
        }

        Ok(())
    }

    #[test]
    fn append_n_commands() -> types::Result<()> {
        let n = 50;
        let dir = tempfile::tempdir().unwrap();

        for i in 0..n {
            let test_key = format!("test_key_{}", i);
            let test_value = format!("test_value_{}", i);

            let comm = match i % 2 {
                0 => Command::Set {
                    key: test_key.clone(),
                    value: test_value.clone(),
                },
                1 => Command::Remove {
                    key: test_key.clone(),
                },
                _ => panic!("I don't even know how this is possible"),
            };

            LogFiles::append_command(dir.path(), &comm)?;

            let state = WorkingState::read_state_file(dir.path())?;
            let log_path = dir.path().join(state.active_log_file_path);

            let mut buf: Vec<u8> = vec![0, 20];
            let mut file = fs::File::open(&log_path).unwrap();
            let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
            let mut handle = reader.take(0);
            let comm_de = Command::deserialize(&mut handle, &mut buf)?;

            if let Command::Set { key, value } = comm_de.clone() {
                std::assert_eq!(i % 2, 0);
                std::assert_eq!(key, test_key);
                std::assert_eq!(value, test_value);

                return Ok(());
            }
            if let Command::Remove { key } = comm_de {
                std::assert_eq!(i % 2, 1);
                std::assert_eq!(key, test_key);

                return Ok(());
            }

            panic!(
                "Deserialized command not Command::{} variant",
                if i % 2 == 0 { "Set" } else { "Remove" }
            );
        }

        Ok(())
    }
}
