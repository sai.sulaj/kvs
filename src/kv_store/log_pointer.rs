use crate::{io_utils, types};
use snafu::ResultExt;
use std::{cmp, convert::TryInto, io, str};

pub const LOG_POINTER_METADATA_BYTES: usize = 56;

/**
 * Serialization format table:
 *
 * BYTES         | PURPOSE
 * 0:8           | byte_offset
 * 8:52          | file_name
 * 52:56         | key_len
 * 56:56+KEY_LEN | key
 *
 * All values are big-endian.
 */
#[derive(Debug, Clone)]
pub struct LogPointer {
    pub byte_offset: u64,
    pub file_name: String,
    pub key: String,
}
impl LogPointer {
    pub fn new(byte_offset: u64, file_name: String, key: String) -> Self {
        if file_name.len() != 44 {
            panic!(
                "file_name must have a length of 44. Current length: {}",
                file_name.len()
            );
        }

        LogPointer {
            byte_offset,
            file_name,
            key,
        }
    }

    pub fn len_bytes(&self) -> usize {
        LOG_POINTER_METADATA_BYTES + self.key.len()
    }

    pub fn serialize(&self, writer: &mut dyn io::Write) -> types::Result<usize> {
        let byte_offset_bytes = self.byte_offset.to_be_bytes();
        io_utils::attempt_write(writer, &byte_offset_bytes)?;
        let file_name_bytes: Vec<u8> = self.file_name.bytes().collect();
        io_utils::attempt_write(writer, &file_name_bytes)?;
        let key_bytes: Vec<u8> = self.key.bytes().collect();
        let key_len: u32 = key_bytes.len() as u32;
        let key_len_bytes = key_len.to_be_bytes();
        io_utils::attempt_write(writer, &key_len_bytes)?;
        io_utils::attempt_write(writer, &key_bytes)?;
        Ok(key_len as usize + LOG_POINTER_METADATA_BYTES)
    }

    pub fn deserialize(
        handle: &mut io::Take<&mut dyn io::BufRead>,
        buf: &mut Vec<u8>,
    ) -> types::Result<(Self, usize)> {
        io_utils::attempt_read(handle, buf, LOG_POINTER_METADATA_BYTES)?;

        let byte_offset = u64::from_be_bytes(buf[0..8].try_into().unwrap());
        let file_name = str::from_utf8(&buf[8..52])
            .context(types::KeyValueInvalidUtf8)?
            .to_string();
        let key_len = u32::from_be_bytes(buf[52..LOG_POINTER_METADATA_BYTES].try_into().unwrap());

        io_utils::attempt_read(handle, buf, key_len as usize)?;
        let key = str::from_utf8(&buf[0..key_len as usize])
            .context(types::KeyValueInvalidUtf8)?
            .to_string();

        let next_log_pointer = LogPointer::new(byte_offset, file_name, key);

        Ok((
            next_log_pointer,
            LOG_POINTER_METADATA_BYTES + key_len as usize,
        ))
    }
}

impl cmp::PartialEq for LogPointer {
    fn eq(&self, other: &Self) -> bool {
        self.byte_offset == other.byte_offset
            && self.file_name == other.file_name
            && self.key == other.key
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        kv_store::{log_files::LogFiles, log_pointer::LogPointer},
        types,
    };
    use snafu::ResultExt;
    use std::{
        self, fs,
        io::{self, prelude::*},
    };
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn ser_de_1() -> types::Result<()> {
        let dir = open_tempdir();
        let file_path = dir.path().join("ser_de_1.test");
        let mut file = fs::File::create(&file_path).unwrap();
        let test_byte_offset = 123u64;
        let test_file_name = LogFiles::generate_log_file_name();
        let test_key = "test_key".to_string();
        let input = LogPointer::new(test_byte_offset, test_file_name.clone(), test_key.clone());
        input.serialize(&mut file)?;

        let mut file = fs::File::open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().clone().to_string() })?;
        let mut reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let mut buf: Vec<u8> = Vec::new();
        let output = LogPointer::deserialize(&mut handle, &mut buf)?.0;

        std::assert_eq!(output.byte_offset, test_byte_offset);
        std::assert_eq!(output.file_name, test_file_name);
        std::assert_eq!(output.key, test_key);

        Ok(())
    }

    #[test]
    fn ser_de_n() -> types::Result<()> {
        let dir = open_tempdir();
        let file_path = dir.path().join("ser_de_n.test");
        let mut file = fs::File::create(&file_path).unwrap();

        let test_file_name = LogFiles::generate_log_file_name();

        for i in 0..10 {
            let test_byte_offset = i * 2u64;
            let test_key = format!("test_key_{}", i);
            let input = LogPointer::new(test_byte_offset, test_file_name.clone(), test_key.clone());
            input.serialize(&mut file)?;
        }

        let mut file = fs::File::open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().clone().to_string() })?;
        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);
        let mut buf: Vec<u8> = Vec::new();

        for i in 0..10 {
            let test_byte_offset = i * 2u64;
            let test_key = format!("test_key_{}", i);
            let output = LogPointer::deserialize(&mut handle, &mut buf)?.0;

            std::assert_eq!(output.byte_offset, test_byte_offset);
            std::assert_eq!(output.file_name, test_file_name);
            std::assert_eq!(output.key, test_key);
        }

        Ok(())
    }
}
