mod command;
mod index_file;
mod index_file_iter;
mod index_file_page;
mod kv_store;
mod log_files;
mod log_file;
mod log_pointer;
mod page_bytes_reserved_file;
mod utils;
mod working_state;
mod log_file_vendor;

pub use kv_store::KvStore;

use crate::{types, KvsEngine};

impl KvsEngine for KvStore {
    fn set(&self, key: String, value: String) -> types::Result<()> {
        KvStore::set(self, key, value)
    }
    fn get(&self, key: String) -> types::Result<Option<String>> {
        KvStore::get(self, &key)
    }
    fn remove(&self, key: String) -> types::Result<()> {
        KvStore::remove(self, key)
    }
}
