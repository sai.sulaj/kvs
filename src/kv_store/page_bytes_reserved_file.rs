use crate::{
    constants,
    kv_store::{index_file::IndexFile, types},
};
use snafu::ResultExt;
use std::{
    collections::HashMap,
    fs,
    io::{self, prelude::*},
    path,
};

type PageBytesReservedMap = HashMap<usize, usize>;

pub fn get_page_reserved_bytes_file_path(dir_path: &path::Path) -> path::PathBuf {
    dir_path.join(constants::file_name::PAGE_CAPACITY)
}

pub struct PageBytesReservedFile<'a> {
    dir_path: &'a path::Path,
    pub page_reserved_bytes_map: PageBytesReservedMap,
}
impl<'a> PageBytesReservedFile<'a> {
    pub fn new(dir_path: &'a path::Path, page_reserved_bytes_map: PageBytesReservedMap) -> Self {
        PageBytesReservedFile {
            dir_path,
            page_reserved_bytes_map,
        }
    }

    fn open_read(&self) -> types::Result<fs::File> {
        let file_path = self.get_path();

        fs::OpenOptions::new()
            .read(true)
            .open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().to_string() })
    }

    fn open_write(&self) -> types::Result<fs::File> {
        let file_path = self.get_path();

        fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().to_string() })
    }

    fn exists(dir_path: path::PathBuf) -> bool {
        let file_path = get_page_reserved_bytes_file_path(&dir_path);

        file_path.is_file()
    }

    pub fn read_or_init(dir_path: &'a path::Path) -> types::Result<Self> {
        if !Self::exists(dir_path.to_path_buf()) {
            return Self::generate(dir_path);
        }

        Self::read(dir_path)
    }

    fn generate(dir_path: &'a path::Path) -> types::Result<PageBytesReservedFile> {
        let index_file = IndexFile::new(dir_path);
        let mut page_reserved_bytes_map: PageBytesReservedMap = HashMap::new();

        let num_pages = index_file.num_pages()?;
        for i in 0..num_pages {
            let page = index_file.get_page(i)?;
            let num_bytes = page
                .read_pointers()?
                .iter()
                .fold(0usize, |acc, curr| acc + curr.len_bytes());
            page_reserved_bytes_map.insert(i, num_bytes);
        }

        Ok(Self::new(dir_path, page_reserved_bytes_map))
    }

    fn read(dir_path: &'a path::Path) -> types::Result<PageBytesReservedFile> {
        let mut page_reserved_bytes_file = PageBytesReservedFile::new(dir_path, HashMap::new());

        let file = page_reserved_bytes_file.open_read()?;
        let reader = io::BufReader::new(file);

        for line in reader.lines() {
            let line = line.context(types::Reading)?;
            let key_value = line
                .splitn(2, '=')
                .map(|s| s.to_string())
                .collect::<Vec<String>>();
            if key_value.len() < 2 {
                continue;
            }
            let page_index: usize = key_value[0]
                .parse::<usize>()
                .expect("Unable to parse page index");
            let page_bytes_reserved: usize = key_value[1]
                .parse::<usize>()
                .expect("Unable to parse page bytes reserved");

            page_reserved_bytes_file.set_page_bytes(page_index, page_bytes_reserved);
        }

        Ok(page_reserved_bytes_file)
    }

    pub fn write(&self) -> types::Result<()> {
        let file = self.open_write()?;
        let mut writer = io::BufWriter::new(file);

        for (page_index, bytes_reserved) in self.page_reserved_bytes_map.iter() {
            let next_str = format!("{}={}\n", page_index, bytes_reserved)
                .bytes()
                .collect::<Vec<u8>>();
            writer.write(&next_str[..]).context(types::Writing)?;
        }

        writer.flush().context(types::Writing)
    }

    fn get_path(&self) -> path::PathBuf {
        get_page_reserved_bytes_file_path(&self.dir_path)
    }

    pub fn set_page_bytes(&mut self, page_index: usize, bytes_reserved: usize) {
        self.page_reserved_bytes_map
            .insert(page_index, bytes_reserved);
    }

    pub fn add_page_bytes(&mut self, page_index: usize, delta: i64) -> types::Result<()> {
        let current = *self.get_page_bytes(page_index).unwrap_or(&0);
        if delta < 0 && delta.abs() as usize > current {
            return types::InvalidArithmetic {
                detail: Some("Attempting to set page reserved bytes to negative value".to_string()),
            }
            .fail();
        }

        let next = if delta < 0 {
            current - (-delta) as usize
        } else {
            current + delta as usize
        };

        self.set_page_bytes(page_index, next);

        Ok(())
    }

    pub fn get_page_bytes(&self, page_index: usize) -> Option<&usize> {
        self.page_reserved_bytes_map.get(&page_index)
    }
}

#[cfg(test)]
mod tests {
    use super::PageBytesReservedFile;
    use crate::types;
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn write_read() -> types::Result<()> {
        let dir = open_tempdir();

        let mut page_reserved_bytes_file = PageBytesReservedFile::read_or_init(dir.path())?;

        for i in 0..5 {
            page_reserved_bytes_file.set_page_bytes(i, i);
        }

        page_reserved_bytes_file.write()?;

        let page_reserved_bytes_file = PageBytesReservedFile::read_or_init(dir.path())?;

        for i in 0..5 {
            let page_bytes = page_reserved_bytes_file.get_page_bytes(i);
            std::assert!(page_bytes.is_some());
            let page_bytes = page_bytes.unwrap();
            std::assert_eq!(*page_bytes, i);
        }

        Ok(())
    }
}
