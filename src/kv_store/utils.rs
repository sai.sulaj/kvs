use crate::{types, constants};
use dotenv_codegen::dotenv;
use std::{fs, time, path};
use snafu::ResultExt;

pub fn get_page_len_bytes() -> types::Result<usize> {
    dotenv!("PAGE_LEN_BYTES").parse::<usize>().map_err(|_| {
        types::InvalidEnv {
            var_name: "PAGE_LEN_BYTES".to_string(),
            message: "Must be a valid integer".to_string(),
        }
        .build()
    })
}

pub fn get_log_file_len_bytes() -> types::Result<usize> {
    dotenv!("LOG_FILE_LEN_BYTES").parse::<usize>().map_err(|_| {
        types::InvalidEnv {
            var_name: "LOG_FILE_LEN_BYTES".to_string(),
            message: "Must be a valid integer".to_string(),
        }
        .build()
    })
}

pub fn get_max_stale_bytes() -> types::Result<usize> {
    dotenv!("MAX_STALE_BYTES").parse::<usize>().map_err(|_| {
        types::InvalidEnv {
            var_name: "MAX_STALE_BYTES".to_string(),
            message: "Must be a valid integer".to_string(),
        }
        .build()
    })
}

pub fn get_log_file_names(dir_path: &path::Path) -> types::Result<Vec<String>> {
    let mut files = fs::read_dir(dir_path)
        .context(types::Reading)?
        .filter_map(|dir_entry_res| {
            dir_entry_res
                .map(|dir_entry| {
                    dir_entry
                        .metadata()
                        .map(|metadata| {
                            if !metadata.is_file() {
                                return None;
                            }
                            metadata.created().map(|created| (dir_entry, created)).ok()
                        })
                        .ok()
                        .flatten()
                })
                .ok()
                .flatten()
        })
        .collect::<Vec<(fs::DirEntry, time::SystemTime)>>();
    files.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
    let files_names = files
        .iter()
        .map(|file| file.0.file_name().to_string_lossy().to_string())
        .filter(|file_name| file_name.ends_with(constants::LOG_EXTENSION))
        .collect::<Vec<String>>();

    Ok(files_names)
}
