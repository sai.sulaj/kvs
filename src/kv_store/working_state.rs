use bson::{self, Bson, Document};
use serde::{Deserialize, Serialize};
use snafu::ResultExt;
use std::{
    collections, fs,
    io::{self, prelude::*},
    path,
};

use crate::{
    constants,
    kv_store::{
        command::Command,
        log_files::LogFiles,
        index_file::IndexFile,
        utils,
    },
    types,
};

// Returns new active log file path.
fn compact(path: &path::Path) -> types::Result<String> {
    let files_names = utils::get_log_file_names(path)?;

    let mut buf: Vec<u8> = Vec::new();
    let mut kvs: collections::HashMap<String, String> = collections::HashMap::new();

    for file_name in &files_names {
        let file_path = path.clone().join(file_name);
        let file = fs::File::open(&file_path);
        if file.is_err() {
            continue;
        }
        let mut file = file.unwrap();
        let reader = &mut io::BufReader::new(&mut file) as &mut dyn io::BufRead;
        let mut handle = reader.take(0);

        while let Ok(command) = Command::deserialize(&mut handle, &mut buf) {
            match command {
                Command::Set { key, value } => kvs.insert(key, value),
                Command::Remove { key } => kvs.remove(&key),
            };
        }
    }

    let log_file_len = utils::get_log_file_len_bytes()?;
    let mut commands: Vec<Command> = Vec::new();
    let mut curr_bytes = 0;
    let mut active_log_file_path: String = LogFiles::generate_log_file_name();
    for (key, value) in kvs.iter() {
        let command = Command::Set {
            key: key.to_string(),
            value: value.to_string(),
        };
        let command_bytes = command.len_bytes() as usize;
        if command_bytes as usize + curr_bytes > log_file_len {
            commands.push(command);
            curr_bytes += command_bytes;
            continue;
        }

        // Flush buf to log file.
        let mut buf: Vec<u8> = Vec::new();
        for command in &commands {
            command.serialize(&mut buf)?;
        }
        let log_file_name = LogFiles::generate_log_file_name();
        active_log_file_path = log_file_name.clone();
        let log_file_path = path.join(log_file_name);
        let mut file = fs::File::create(&log_file_path)
            .context(types::OpenFile { file_name: log_file_path.to_string_lossy().to_string() })?;
        file.write_all(&buf).context(types::Writing)?;

        // Reset iteration state.
        commands.clear();
        curr_bytes = 0;
    }

    // Clean up old log files.
    for file_name in files_names {
        let file_path = path.clone().join(file_name);
        fs::remove_file(file_path).expect("Unable to delete log file");
    }

    // Page capacity map file and index file is now stale.
    fs::remove_file(path.join(constants::file_name::PAGE_CAPACITY))
        .expect("Unable to delete page capacity map file");
    fs::remove_file(path.join(constants::file_name::INDEX))
        .expect("Unable to delete page capacity map file");

    let index_file = IndexFile::new(path);
    index_file.generate()?;

    Ok(active_log_file_path)
}

#[derive(Serialize, Deserialize)]
pub struct WorkingState {
    pub active_log_file_path: String,
    pub stale_bytes: isize,
}
impl WorkingState {
    pub fn new(active_log_file_path: String, stale_bytes: isize) -> Self {
        WorkingState {
            active_log_file_path,
            stale_bytes,
        }
    }

    pub fn generate_new(stale_bytes: isize) -> Self {
        let next_logfile_name = LogFiles::generate_log_file_name();
        WorkingState::new(next_logfile_name, stale_bytes)
    }

    pub fn read_state_file(dir_path: &path::Path) -> types::Result<Self> {
        let file_path = dir_path.join(constants::file_name::STATE);
        let mut file = fs::OpenOptions::new()
            .read(true)
            .open(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().to_string() })?;
        let mut reader = io::BufReader::new(&mut file);

        let doc: Document =
            bson::Document::from_reader(&mut reader).context(types::DeserializingWorkingState)?;
        let working_state: WorkingState =
            bson::from_bson(bson::Bson::Document(doc)).context(types::DeserializingWorkingState)?;

        Ok(working_state)
    }

    pub fn write_state_file(&mut self, dir_path: &path::Path) -> types::Result<()> {
        let max_stale_bytes = utils::get_max_stale_bytes()? as isize;
        if self.stale_bytes > max_stale_bytes {
            println!("Compacting...");
            self.active_log_file_path = compact(dir_path)?;
            self.stale_bytes = 0;
        }
        let file_path = dir_path.join(constants::file_name::STATE);
        let mut file = fs::File::create(&file_path)
            .context(types::OpenFile { file_name: file_path.to_string_lossy().to_string() })?;
        let mut writer = io::BufWriter::new(&mut file);

        let doc: Document = match bson::to_bson(&self).context(types::SerializingWorkingState)? {
            Bson::Document(doc) => doc,
            _ => panic!("Serialized working state is not a Document"),
        };

        doc.to_writer(&mut writer)
            .context(types::SerializingWorkingState)?;
        Ok(())
    }

    pub fn state_file_exists(dir_path: &path::Path) -> bool {
        let file_path = dir_path.join(constants::file_name::STATE);
        file_path.exists()
    }

    pub fn read_or_init(dir_path: &path::Path) -> types::Result<Self> {
        if WorkingState::state_file_exists(dir_path) {
            return WorkingState::read_state_file(dir_path);
        }

        Ok(Self::generate_new(0))
    }
}

#[cfg(test)]
mod tests {
    use crate::{kv_store::working_state::WorkingState, types};
    use tempfile;

    fn open_tempdir() -> tempfile::TempDir {
        tempfile::tempdir().expect("Unable to open temporary working directory")
    }

    #[test]
    fn read_write_state_file() -> types::Result<()> {
        let dir = open_tempdir();
        let active_log_file_path = "active_log_file_path".to_string();
        let stale_bytes = 123;

        let mut working_state = WorkingState::new(active_log_file_path.clone(), stale_bytes);
        working_state.write_state_file(dir.path())?;
        let res = WorkingState::read_state_file(dir.path());

        std::assert!(res.is_ok());
        let res = res.unwrap();
        std::assert_eq!(res.active_log_file_path, active_log_file_path);
        std::assert_eq!(res.stale_bytes, stale_bytes);

        Ok(())
    }
}
