use crate::Result;

/// This trait provides a generic interface for the KvStore. This way different engines can be used
/// interchangeably.
pub trait KvsEngine: Clone + Send + 'static {
    /// Sets a key-value pair.
    ///
    /// Errors
    /// Panics if unable to update any of the relevant files (permission errors for example).
    fn set(&self, key: String, value: String) -> Result<()>;
    /// Gets the value of the specified key. None if unset.
    ///
    /// Errors
    /// Panics if unable to update any of the relevant files (permission errors for example).
    fn get(&self, key: String) -> Result<Option<String>>;
    /// Deletes a key-value pair.
    ///
    /// Errors
    /// Panics if unable to update any of the relevant files (permission errors for example).
    fn remove(&self, key: String) -> Result<()>;
}
