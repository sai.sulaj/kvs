#![deny(missing_docs)]

//! # KvStore is a simple log-structured key-value store.
//!
//! Each command is appended to log files. A simple index is maintained that is used to point to
//! commands in the log files.
//!
//! # Files
//! * <hash>.kvs.log -> Log files.
//! * .index.kvs -> The files containing the index.
//! * .state.kvs -> The current state. This contains the current active log file, as well as the
//!     number of stale bytes (bytes in all log files that do not contain a log pointer
//!     in the index).
//! * .page_capacity.kvs -> A map of index file page indices and the number of bytes contained in
//!     them.
//!
//! # The Index File
//! The index file uses pages to speed up updates. The page size is set in the .env file.
//!
//! # TODO
//! * Concurrency.
//! * Some error variants are either generic or just plain wrong. Needs to be updated.

mod constants;
mod io_utils;
/// The primary KvStore interface.
pub mod kv_store;
mod kvs_engine;
mod types;

pub use kv_store::KvStore;
pub use kvs_engine::KvsEngine;
pub use types::{Error, Result};
