use bson;
use snafu::Snafu;
use std::{io, str};

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
/// This enum is used to represent all errors in this crate.
pub enum Error {
    /**
     * Thrown if kvs attempted to be opened in an invalid
     * directory.
     */
    #[snafu(display("Invalid working directory: {}", source))]
    InvalidWorkingDir {
        /// The underlying IO error.
        source: io::Error,
    },
    /**
     * Thrown if kvs attempted to be opened in an invalid
     * directory.
     */
    #[snafu(display("Invalid working directory: Path is not a directory"))]
    WorkingDirNotDir {},
    /**
     * Thrown if unable to read all necessary bytes from
     * a reader.
     */
    #[snafu(display("Unable to read all necessary bytes"))]
    ReadAllBytes {},
    /**
     * Thrown if unable to write all necessary bytes from
     * a reader.
     */
    #[snafu(display("Unable to write all necessary bytes"))]
    WriteAllBytes {},
    /**
     * Thrown if a key or value contains invalid UTF-8.
     */
    #[snafu(display("Unable to write all necessary bytes"))]
    KeyValueInvalidUtf8 {
        /// The underlying Utf8 error.
        source: str::Utf8Error,
    },
    /**
     * Thrown if unable to open/create a file.
     */
    #[snafu(display("Unable to open/create file: {} -> {}", file_name, source))]
    OpenFile {
        /// The underlying IO error.
        source: io::Error,
        /// The name of the file attempted to be opened.
        file_name: String,
    },
    /**
     * Thrown if unable to deserialize WorkingState
     */
    #[snafu(display("Unable to deserialize working state: {}", source))]
    DeserializingWorkingState {
        /// The underlying deserialization error.
        source: bson::de::Error,
    },
    /**
     * Thrown if unable to serialize WorkingState
     */
    #[snafu(display("Unable to serialize working state: {}", source))]
    SerializingWorkingState {
        /// The underlying serialization error.
        source: bson::ser::Error,
    },
    /**
     * Wraps any errors during writing to a writer.
     */
    #[snafu(display("Unable to write bytes: {}", source))]
    Writing {
        /// The underlying IO error.
        source: io::Error,
    },
    /**
     * Wraps any errors during reading to a reader.
     */
    #[snafu(display("Unable to read bytes: {}", source))]
    Reading {
        /// The underlying IO error.
        source: io::Error,
    },
    /**
     * Thrown if environment is invalid.
     */
    #[snafu(display("Invalid environment configuration [{}]: {}", var_name, message))]
    InvalidEnv {
        /// The invalid variable name.
        var_name: String,
        /// A hint to fix it.
        message: String,
    },
    /**
     * Thrown if write is attempted with too many bytes.
     */
    #[snafu(display("Unable to perform write: too many bytes."))]
    WriteOverflow {},
    /**
     * Thrown if write is attempted with too many bytes.
     */
    #[snafu(display("Invalid arithmetic{}", detail.clone().map(|m| { format!(": {}", m) }).unwrap_or("".to_string())))]
    InvalidArithmetic {
        /// Error details.
        detail: Option<String>,
    },
}

/// The result type used in thise crate.
pub type Result<A = (), E = Error> = std::result::Result<A, E>;
