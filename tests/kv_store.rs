use std;

use kvs::{kv_store::KvStore, Result};
use tempfile;

fn open_tempdir() -> tempfile::TempDir {
    tempfile::tempdir().expect("Unable to open temporary working directory")
}

#[test]
fn open_valid_working_dir() -> Result<()> {
    let dir = open_tempdir();
    KvStore::open(dir.path())?;

    Ok(())
}

#[test]
fn error_on_open_non_dir() -> Result<()> {
    let dir = open_tempdir();
    let file_path = dir.path().join("temp_file.txt");
    std::fs::File::create(&file_path).expect("Unable to open temporary file");
    let res = KvStore::open(file_path.as_path());

    std::assert!(res.is_err());

    Ok(())
}

// #[test]
// fn get_set_values() -> Result<()> {
// let dir = open_tempdir();

// let store = KvStore::open(dir.path())?;

// store.set("k1".to_string(), "v1".to_string())?;
// store.set("k2".to_string(), "v2".to_string())?;

// let v1 = store.get("k1".to_string())?;
// let v2 = store.get("k2".to_string())?;

// std::assert!(v1.is_some());
// std::assert!(v2.is_some());
// std::assert_eq!(v1.unwrap(), "v1".to_string());
// std::assert_eq!(v2.unwrap(), "v2".to_string());

// Ok(())
// }
